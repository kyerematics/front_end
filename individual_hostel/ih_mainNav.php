<header class="site-header header-default header-sticky">
    <nav class="main-navigation">
        <div class="vk-main-menu animated uni-sticky">
            <div class="container">
                <div class="uni-transparent-1-menu">
                    <!--./vk-navbar-header-->

                    <div class="row">
                        <div class="collapse navbar-collapse vk-navbar-collapse" id="menu">
                            <nav class="main-navigation">
                                <div class="inner-navigation">
                                    <ul class="vk-navbar-nav vk-navbar-left nav-bar">

                                        <li><a href="#">Hostel Name</a>

                                        </li>

                                    </ul>
                                    <ul class="vk-navbar-nav vk-navbar-right">

                                        <li><a href="#overview">Overview</a>

                                        </li>
                                        <li><a href="#room">Room Type</a>

                                        </li>
                                        <li><a href="#amenities">Amenities</a>
                                        </li>
                                        <li><a href="#location">Location</a>
                                        </li>
                                        <li><a href="individual_hostel/individual_hostel_R&C.php">Reviews and Comments</a>
                                        </li>

                                        <li class="vk-icon-search"><i class="fa fa-search" aria-hidden="true"></i></li>

                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <!--./vk-navbar-collapse-->

                    <!--search-->
                    <div class="box-search-header collapse in" id="box-search-header">
                        <div class="vk-input-group">
                            <div class="input-group stylish-input-group">
                                <input type="text" class="form-control"  placeholder="I am here to help" >
                                <span class="input-group-addon">
                                        <button type="submit">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="vk-header-top hidden-xs hidden-sm">
        <div class="container">
            <div class="content">
                <ul class="quick-address">
                    <li ><a href="#">REGISTER</a> </li>
                    <li ><a href="../signin.php">SIGN IN</a> </li>
                </ul>
                <div class="quick-address vk-navbar-left">


                    <nav class="main-navigation">
                        <div class="inner-navigation">
                            <ul class="nav-bar">
                                <li><a href="../index.php">Home</a>

                                </li>
                                <li><a href="../general_hostels.php">Hostels</a>
                                </li>
                                <li><a href="../general_hostels.php">Homestels</a>
                                </li>
<!--                                <li><a href="#">Halls</a>-->
<!--                                </li>-->
                                <li><a href="../manager_signup.php">Add your hostel</a>
                                </li>
                                <li><a href="#">Help</a>
                                </li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>