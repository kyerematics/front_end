<!DOCTYPE html>
<html lang="en">
<head>
    <title>E-hostelry | Manager Sign up</title>

    <?php include('_partials/head.php'); ?>

</head>

<body>


<?php include('_partials/mobileNav.php'); ?>

<div id="wrapper-container" class="site-wrapper-container">


<?php include('_partials/mainNav.php'); ?>


<div id="main-content" class="site-main-content">
    <div id="home-main-content" class="site-home-main-content">

        <section class="site-content-area">
            <div class="vk-gallery-grid-full-banner">
                <div class="vk-about-banner">
                    <div class="vk-about-banner-destop">
                        <div class="vk-banner-img"></div>
                        <div class="vk-about-banner-caption">
                            <h2>Sign Up</h2>
                            <h3>
                                <a href="#">Add your hostel by first signing up</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="vk-shop-checkout-body">
                <div class="container">
                    <main id="main" class="clearfix right_sidebar">
                        <div class="tg-container">
                            <div id="primary">


                                <div class="entry-thumbnail">

                                    <div class="entry-content-text-wrapper clearfix">
                                        <div class="entry-content-wrapper">
                                            <div class="entry-content">
                                                <div class="woocommerce">
                                                    <div class="woocommerce-info">
                                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                        <div class="pull-right">
                                                            I am a Hostel Manager &nbsp;
                                                            <label class="switch switch-icon switch-success-outline-alt">
                                                                <input type="checkbox" class="switch-input" checked="">
                                                                <span class="switch-label" data-on="" data-off=""></span>
                                                                <span class="switch-handle"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="vk-checkout-login">
                                                        <div class="row">
                                                            <form class="woocomerce-form woocommerce-form-login login" method="post" >

                                                                <div class="col-md-12">
                                                                    <p class="form-row form-row-first">
                                                                        <label for="username" class=""> Email <abbr class="required" title="required">*</abbr></label>
                                                                        <input type="text" class="input-text" name="username" id="username" placeholder="Please enter your email">
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <p class="form-row form-row-last">
                                                                        <label for="password" class="password"> Password <abbr class="required" title="required">*</abbr></label>
                                                                        <input class="input-text" type="password" name="password" id="password" placeholder="Please enter your password">
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <p class="form-row form-row-last">
                                                                        <label for="confirm_password" class="password"> Confirm Password <abbr class="required" title="required">*</abbr></label>
                                                                        <input class="input-text" type="password" name="password" id="confirm_password" placeholder="Please confirm your password">
                                                                    </p>
                                                                </div>
                                                                <div class="clear"></div>


                                                            
                                                                            <div class="form-row place-order">
                                                                                    <noscript>
                                                                                        Since your browser does not support JavaScript, or it is disabled, please ensure you click the &lt;em&gt;Update Totals&lt;/em&gt; button before placing your order. You may be charged more than the amount stated above if you fail to do so.          &lt;br/&gt;&lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" /&gt;
                                                                                    </noscript>


                                                                                    <p>
                                                                                        <input type="submit" class="button alt" name="manager_signup" id="register" value="Register" data-value="register" formmethod="post" formaction="hostel_registration/index.php" />
                                                                                    </p>
                                                                                </div>
                                                            

                                                                <div class="clear"></div>
                                                            </form>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>


                                                </div>
                                            </div><!-- .entry-content -->
                                        </div>
                                    </div>
                                </div>


                            </div> <!-- Primary end -->
                        </div>
                    </main>
                </div>
            </div>
        </section>


    </div>
</div>



</div>
<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery1.min.js"></script>
<script src="../plugin/dist/owl.carousel.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/number-count/jquery.counterup.min.js"></script>
<script src="../js/isotope.pkgd.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/bootstrap-datepicker.min.js"></script>
<script src="../js/bootstrap-datepicker.tr.min.js"></script>
<script src="../js/moment.min.js"></script>
<script src="../js/wow.min.js"></script>
<script src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script src="../js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="../js/picturefill.min.js"></script>
<script src="../js/lightgallery.js"></script>
<script src="../js/lg-pager.js"></script>
<script src="../js/lg-autoplay.js"></script>
<script src="../js/lg-fullscreen.js"></script>
<script src="../js/lg-zoom.js"></script>
<script src="../js/lg-hash.js"></script>
<script src="../js/lg-share.js"></script>
<script src="../js/jquery.nice-select.js"></script>
<script src="../js/semantic.js"></script>
<script src="../js/parallax.min.js"></script>
<script src="../js/jquery.nicescroll.min.js"></script>
<script src="../js/jquery.sticky.js"></script>
<script src="../js/main.js"></script>
</body>
</html>