<!DOCTYPE html>
<html lang="en">
<?php include "_partials/head.php";?>
<body class="" onload="loadvideo()">

<!--load page-->
<div class="load-page">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>

<!-- Mobile nav -->
<?php include "_partials/mobileNav.php";?>
<!-- End mobile menu -->

<div id="wrapper-container" class="clearfix site-wrapper-container">
    <?php include "_partials/mainNav.php";?>
    <!--End of navigation-->



    <div id="main-content" class="site-main-content">
        <div id="home-main-content" class="site-home-main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="vk-slide">
                        <div id="owl-slide-home" class="owl-carousel owl-theme animated">
                            <div class="item animated">
                                <div class="vk-item-slide">
                                    <div class="slider-video">
                                        <video id="video-demo" loop>
                                            <source src="../images/01_01_default/slide-vide/720P.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                    <div class="vk-item-slide-btn">
                                        <ul>
                                            <li>
                                                <button class="vk-play" onclick="playPause()"></button>
                                                <button class="vk-pause" onclick="playPause()"></button>
                                            </li>
                                            <li>
                                                <button class="vk-mutesound" onclick="enableMute()" type="button"></button>
                                                <button class="vk-enablemute" onclick="disableMute()" type="button"></button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="vk-slide-caption">
                                        <h3 class="animated  fadeInDown slide-delay-1">Hostel Slang</h3>
                                        <h2 class="animated fadeInUp slide-delay-2 ">Hostel Name</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="item  animated">
                                <div class="vk-item-slide">
                                    <img src="../images/01_01_default/banner.jpg" alt="" class="img-responsive">
                                    <div class="vk-slide-caption animated">
                                        <h3 class="animated rotateInDownRight slide-delay-1">Hostel Slang</h3>
                                        <h2 class="animated fadeInUpBig slide-delay-2">Hostel Name</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="item  animated">
                                <div class="vk-item-slide">
                                    <img src="../images/01_01_default/banner2.jpg" alt="" class="img-responsive">
                                    <div class="vk-slide-caption animated">
                                        <h3 class="animated flipInX slide-delay-1">Hostel Slang</h3>
                                        <h2 class="animated fadeInUp slide-delay-2">Hostel name</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Advanced search-->

                    <div class="vk-booking-hotel">
                        <div class="container-fluid">
                            <form action="#" class="form-horizontal  booking-hotel-all">
                                <ul>

                                    <li>
                                        <h4>Advanced Search</h4>
                                    </li>
                                    <li>
                                        <h4>Hostel</h4>
                                        <div class="input-group">
                                            <input name="hostelName" title="#" class="form-control" type="text" value="">
                                            <span class="input-group-addon btn"><span class="fa fa-building" id="ti-calendar2"></span></span>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>Location</h4>
                                        <div class="wrap-select">
                                            <div id="dd" class="wrapper-dropdown-3">
                                                <small>Choose your location</small>
                                                <ul class="dropdown">
                                                    <li><a href="#">Ayeduase</a></li>
                                                    <li><a href="#">Ayigya</a></li>
                                                    <li><a href="#">Bomso</a></li>
                                                    <li><a href="#">Kentikrono</a></li>
                                                    <li><a href="#">Kotei</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>Price Filter</h4>
                                        <div class="wrap-select">
                                            <div class="vk-newlist-title2" style="margin-top: 1px;">
                                                <div id="slider-range" style="margin-top: 5px">
                                                    <label class="label-filter-price"><input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;"></label>
                                                </div>
                                                 </div>
                                        </div>


                                    </li>
                                    <li>
                                        <div class="vk-btn-check">
                                            <button formaction="search_results.php" type="submit" class="btn vk-btn-primary btn-block btn-check">Advanced Search</button>
                                        </div>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>


<!--Advanced search-->

    <!--List of hostels-->

                    <!--BENGIN CONTENT HEADER-->
                    <div class="clearfix"></div>
                    <section class="site-content-area">
                        <div class="container-fluid">
                            <div class="row">

                                <div class="vk-room-list-content">
                                    <div class="container">
                                        <div class="vk-room-list-header">
                                            <h3>Available Hostels</h3>
                                            <h2>Choose A Hostel</h2>
                                            <div class="vk-room-list-border"></div>
                                        </div>
                                        <!--Hostel 1-->

                                        <div class="row">
                                            <div class="item">
                                                <div class="col-md-6 vk-dark-our-room-item-left  vk-clear-padding">
                                                    <div class="vk-dark-our-room-item-img">
                                                        <img src="../images/04_02_room_grid/img-1.jpg" alt="" class="img-responsive">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 vk-dark-our-room-item-right vk-clear-padding">
                                                    <div class="vk-dark-our-room-item-content">
                                                        <h3><a href="../individual_hostel">Hostel Name</a></h3>
                                                        <ul>
                                                            <li><p><i class="fa fa-diamond" aria-hidden="true"></i> Hostel slang <span> : Brunei</span></p></li>
                                                            <li><p><i class="fa fa-address-card" aria-hidden="true"></i> Location <span> : 1 Hostel Address</span></p></li>
                                                            <li>
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <div class="col-xs-3"><p><i class="fa fa-arrows-alt" aria-hidden="true"></i><small>Room Type</small></p>
                                                                        <ul>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                        </ul></div>
                                                                        <div class="col-xs-3"><p><i class="" aria-hidden="true"></i>&cent; Price</p>
                                                                            <ul>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                            </ul></div>
                                                                        <div class="col-xs-3"><p><i class="fa fa-male" aria-hidden="true"></i> Male</p>
                                                                            <ul>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                            </ul></div>
                                                                        <div class="col-xs-3"><p><i class="fa fa-female" aria-hidden="true"></i> Female</p>
                                                                            <ul>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                            </ul></div>

                                                                    </div>
                                                            </div>
                                                            </ul>
                                                        <div class="vk-dark-our-room-item-book">
                                                            <div class="vk-dark-our-room-item-book-left">
                                                                <ul>
                                                                    <li>
                                                                        <p>Rating : </p>
                                                                    </li>
                                                                    <li>
                                                                        <p>8/ <span>10</span></p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="vk-dark-our-room-item-book-right">
                                                                <a href="../booking"> Rent A Bed <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                        <!--Hostel 1-->
                                        <!--Hostel 1-->

                                        <div class="row">
                                            <div class="item">
                                                <div class="col-md-6 vk-dark-our-room-item-left  vk-clear-padding">
                                                    <div class="vk-dark-our-room-item-img">
                                                        <img src="../images/04_02_room_grid/img-2.jpg" alt="" class="img-responsive">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 vk-dark-our-room-item-right vk-clear-padding">
                                                    <div class="vk-dark-our-room-item-content">
                                                        <h3><a href="../individual_hostel">Hostel Name</a></h3>
                                                        <ul>
                                                            <li><p><i class="fa fa-diamond" aria-hidden="true"></i> Hostel slang <span> : Brunei</span></p></li>
                                                            <li><p><i class="fa fa-address-card" aria-hidden="true"></i> Location <span> : 1 Hostel Address</span></p></li>
                                                            <li>
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <div class="col-xs-3"><p><i class="fa fa-arrows-alt" aria-hidden="true"></i><small>Room Type</small></p>
                                                                        <ul>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                        </ul></div>
                                                                        <div class="col-xs-3"><p><i class="" aria-hidden="true"></i>&cent; Price</p>
                                                                            <ul>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                            </ul></div>
                                                                        <div class="col-xs-3"><p><i class="fa fa-male" aria-hidden="true"></i> Male</p>
                                                                            <ul>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                            </ul></div>
                                                                        <div class="col-xs-3"><p><i class="fa fa-female" aria-hidden="true"></i> Female</p>
                                                                            <ul>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                            </ul></div>

                                                                    </div>
                                                            </div>
                                                            </ul>
                                                        <div class="vk-dark-our-room-item-book">
                                                            <div class="vk-dark-our-room-item-book-left">
                                                                <ul>
                                                                    <li>
                                                                        <p>Rating : </p>
                                                                    </li>
                                                                    <li>
                                                                        <p>8/ <span>10</span></p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="vk-dark-our-room-item-book-right">
                                                                <a href="../booking"> Rent A Bed <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                        <!--Hostel 1-->
                                        <!--Hostel 1-->

                                        <div class="row">
                                            <div class="item">
                                                <div class="col-md-6 vk-dark-our-room-item-left  vk-clear-padding">
                                                    <div class="vk-dark-our-room-item-img">
                                                        <img src="../images/04_02_room_grid/img-3.jpg" alt="" class="img-responsive">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 vk-dark-our-room-item-right vk-clear-padding">
                                                    <div class="vk-dark-our-room-item-content">
                                                        <h3><a href="../individual_hostel">Hostel Name</a></h3>
                                                        <ul>
                                                            <li><p><i class="fa fa-diamond" aria-hidden="true"></i> Hostel slang <span> : Brunei</span></p></li>
                                                            <li><p><i class="fa fa-address-card" aria-hidden="true"></i> Location <span> : 1 Hostel Address</span></p></li>
                                                            <li>
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <div class="col-xs-3"><p><i class="fa fa-arrows-alt" aria-hidden="true"></i><small>Room Type</small></p>
                                                                        <ul>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                           <li><p class="text text-center"><span>1</span></p></li>
                                                                        </ul></div>
                                                                        <div class="col-xs-3"><p><i class="" aria-hidden="true"></i>&cent; Price</p>
                                                                            <ul>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                                <li><p class="text text-center"><span>&cent;1500</span></p></li>
                                                                            </ul></div>
                                                                        <div class="col-xs-3"><p><i class="fa fa-male" aria-hidden="true"></i> Male</p>
                                                                            <ul>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                            </ul></div>
                                                                        <div class="col-xs-3"><p><i class="fa fa-female" aria-hidden="true"></i> Female</p>
                                                                            <ul>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                                <li><p class="text text-center"><span>1</span></p></li>
                                                                            </ul></div>

                                                                    </div>
                                                            </div>
                                                            </ul>
                                                        <div class="vk-dark-our-room-item-book">
                                                            <div class="vk-dark-our-room-item-book-left">
                                                                <ul>
                                                                    <li>
                                                                        <p>Rating : </p>
                                                                    </li>
                                                                    <li>
                                                                        <p>8/ <span>10</span></p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="vk-dark-our-room-item-book-right">
                                                                <a href="../booking"> Rent A Bed <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                        <!--Hostel 1-->


                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--END CONTENT ABOUT-->
    <!--List of hostels-->
                </div>
                <!--Pagination-->
                <div class="vk-select-room-pagination">
                    <ul>
                        <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i> PREV</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">NEXT <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <!--Pagination-->
                <div class="vk-divider vk-divider-dashed"><p class="vk-divider-2px-double"></p></div>
            </div>
        </div>
    </div>




    <?php include "_partials/footer.php";?>

</div>
<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery1.min.js"></script>
<script src="../plugin/dist/owl.carousel.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/number-count/jquery.counterup.min.js"></script>
<script src="../js/isotope.pkgd.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/bootstrap-datepicker.min.js"></script>
<script src="../js/bootstrap-datepicker.tr.min.js"></script>
<script src="../js/moment.min.js"></script>
<script src="../js/wow.min.js"></script>
<script src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script src="../js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="../js/picturefill.min.js"></script>
<script src="../js/lightgallery.js"></script>
<script src="../js/lg-pager.js"></script>
<script src="../js/lg-autoplay.js"></script>
<script src="../js/lg-fullscreen.js"></script>
<script src="../js/lg-zoom.js"></script>
<script src="../js/lg-hash.js"></script>
<script src="../js/lg-share.js"></script>
<script src="../js/jquery.nice-select.js"></script>
<script src="../js/semantic.js"></script>
<script src="../js/parallax.min.js"></script>
<script src="../js/jquery.nicescroll.min.js"></script>
<script src="../js/jquery.sticky.js"></script>
<script src="../js/main.js"></script>
</body>
</html>
