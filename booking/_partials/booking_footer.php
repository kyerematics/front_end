<footer class="site-footer footer-default">
    <div class="footer-main-content">
        <div class="container">
            <div class="row">
                <div class="footer-main-content-elements">
                    <div class="footer-main-content-element col-sm-4">
                        <aside class="widget">
                            <div class="vk-widget-footer-1">
                                <div class="widget-title">
                                    <a href="#"><img src="../../images/logo-footer.png" alt="" class="img-responsive"></a>
                                </div>
                                <div class="widget-content">
                                    <div class="vk-widget-content-info">
                                        <p><span class="ti-mobile"></span> (+233) 123 456789</p>
                                        <p><span class="ti-email"></span> contact@sparta.com</p>
                                        <p><span class="ti-location-pin"></span> 45 Queen's Park Rd, Brighton, BN2 0GJ, UK</p>
                                    </div>
                                    <div class="vk-widget-content-share">
                                        <p>
                                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </aside>
                    </div>
                    <div class="footer-main-content-element col-sm-2">
                        <aside class="widget">
                            <div class="vk-widget-footer-2">
                                <h3 class="widget-title"> Company</h3>
                                <div class="widget-content">
                                    <ul class="vk-widget-content-2">
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Home</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> About us</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Testimonial</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Blog</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="footer-main-content-element col-sm-2">
                        <aside class="widget">
                            <div class="vk-widget-footer-3">
                                <h3 class="widget-title">Services</h3>
                                <div class="widget-content">
                                    <ul class="vk-widget-content-2">
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Spa</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Rooms</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Restaurant</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Gym</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Promotion</a></li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="footer-main-content-element col-sm-4">
                        <aside class="widget">
                            <div class="vk-widget-footer-4">
                                <h3 class="widget-title">Sign up for newletter</h3>
                                <div class="widget-content">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="email" class="form-control" placeholder="Your Email...">
                                            <span class="input-group-addon success"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                    <div class="vk-widget-trip">
                                        <a href="#"><img src="../../images/01_01_default/stripad.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-area">
        <div class="container">
            <div class="copyright-content">
                <div class="row">
                    <div class="col-sm-6">
                        <p class="copyright-text">
                            <span>Sparta Plaza</span> -  Hotel & Resort PSD Template. Design by <span><a href="#">UniverTheme</a></span>
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <ul class="copyright-menu">
                            <li><a href="#">Term & Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Site Map</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>