<!doctype html>
<html lang="en">
<?php include('_partials/booking_head.php');?>
<body>

<!--load page-->
<div class="load-page">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>

<div class="vk-sparta-transparents-1">
    <!-- Mobile nav -->
    <?php include('_partials/booking_mobileNav.php');?>
    <!-- End mobile menu -->

    <div id="wrapper-container" class="site-wrapper-container">
        <!-- Main navigation -->
        <?php include('_partials/booking_mainNav.php');?>
        <!-- end of Main navigation -->



        <!--BENGIN CONTENT HEADER-->
        <section class="site-content-area">
            <div class="vk-gallery-grid-full-banner">
                <div class="vk-about-banner">
                    <div class="vk-about-banner-destop">
                        <div class="vk-banner-img"></div>
                        <div class="vk-about-banner-caption">
                            <h2>Reserve a bed</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="vk-confirmation-content">
                <div class="container">
                    <div class="vk-select-room-breakcrumb">
                        <ul>
                            <li class="completed">
                                <a href="javascript:void(0);">1. Personal Information</a>
                            </li>
                            <li class="active">
                                <a href="javascript:void(0);">2. Hostel</a>
                                <span class="round-tabs five">
                             <i class="fa fa-check" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li class="make-a-reservation">
                                <a href="javascript:void(0);">3. Choose a room</a>
                                <span class="round-tabs five">
                              <i class="fa fa-check" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li class="confirmation">
                                <a href="javascript:void(0);">4. Payment</a>
                                <span class="round-tabs five">
                             <i class="fa fa-check" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li class="confirmation">
                                <a href="javascript:void(0);">5. Confirmation</a>
                                <span class="round-tabs five">
                             <i class="fa fa-check" aria-hidden="true"></i>
                         </span>
                            </li>
                        </ul>
                    </div>
                    <div class="vk-shop-checkout-body">




                        <div class="vk-checkout-order-paypal">
                            <div class="row">
                                <div id="order_review" class="woocommerce-checkout-review-order">



                                    <div class="col-md-12">
                                        <div id="payment" class="woocommerce-checkout-payment">

                                            <div class="form-row place-order">
                                                <noscript>
                                                    Since your browser does not support JavaScript, or it is disabled, please ensure you click the &lt;em&gt;Update Totals&lt;/em&gt; button before placing your order. You may be charged more than the amount stated above if you fail to do so.			&lt;br/&gt;&lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" /&gt;
                                                </noscript>


                                                <div class="col-xs-6">
                                                    <a href="booking_payment.php"><input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="previous" value="Previous" data-value="Previous"></a>
                                                    </div>
                                                <div class="col-xs-6">
                                                    <a href="#"><input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="next" value="Submit" data-value="submit"></a>
                                                    </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                </div>


            </div>
        </section>
        <!--END CONTENT ABOUT-->


        <footer class="site-footer footer-default">
            <div class="footer-main-content">
                <div class="container">
                    <div class="row">
                        <div class="footer-main-content-elements">
                            <div class="footer-main-content-element col-sm-4">
                                <aside class="widget">
                                    <div class="vk-widget-footer-1">
                                        <div class="widget-title">
                                            <a href="#"><img src="../images/logo-footer.png" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="widget-content">
                                            <div class="vk-widget-content-info">
                                                <p><span class="ti-mobile"></span> (+233) 123 456789</p>
                                                <p><span class="ti-email"></span> contact@sparta.com</p>
                                                <p><span class="ti-location-pin"></span> 45 Queen's Park Rd, Brighton, BN2 0GJ, UK</p>
                                            </div>
                                            <div class="vk-widget-content-share">
                                                <p>
                                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                    <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                </aside>
                            </div>
                            <div class="footer-main-content-element col-sm-2">
                                <aside class="widget">
                                    <div class="vk-widget-footer-2">
                                        <h3 class="widget-title"> Company</h3>
                                        <div class="widget-content">
                                            <ul class="vk-widget-content-2">
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Home</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> About us</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Testimonial</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Blog</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Contact</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                            <div class="footer-main-content-element col-sm-2">
                                <aside class="widget">
                                    <div class="vk-widget-footer-3">
                                        <h3 class="widget-title">Services</h3>
                                        <div class="widget-content">
                                            <ul class="vk-widget-content-2">
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Spa</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Rooms</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Restaurant</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Gym</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Promotion</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                            <div class="footer-main-content-element col-sm-4">
                                <aside class="widget">
                                    <div class="vk-widget-footer-4">
                                        <h3 class="widget-title">Sign up for newletter</h3>
                                        <div class="widget-content">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="email" class="form-control" placeholder="Your Email...">
                                                    <span class="input-group-addon success"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="vk-widget-trip">
                                                <a href="#"><img src="../images/01_01_default/stripad.png" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <div class="container">
                    <div class="copyright-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="copyright-text">
                                    <span>Sparta Plaza</span> -  Hotel & Resort PSD Template. Design by <span><a href="#">UniverTheme</a></span>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <ul class="copyright-menu">
                                    <li><a href="#">Term & Conditions</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Site Map</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>
    <!-- Latest compiled and minified JavaScript -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery1.min.js"></script>
    <script src="../plugin/dist/owl.carousel.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.waypoints.js"></script>
    <script src="../js/number-count/jquery.counterup.min.js"></script>
    <script src="../js/isotope.pkgd.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/bootstrap-datepicker.min.js"></script>
    <script src="../js/bootstrap-datepicker.tr.min.js"></script>
    <script src="../js/moment.min.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script src="../js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
    <script src="../js/picturefill.min.js"></script>
    <script src="../js/lightgallery.js"></script>
    <script src="../js/lg-pager.js"></script>
    <script src="../js/lg-autoplay.js"></script>
    <script src="../js/lg-fullscreen.js"></script>
    <script src="../js/lg-zoom.js"></script>
    <script src="../js/lg-hash.js"></script>
    <script src="../js/lg-share.js"></script>
    <script src="../js/jquery.nice-select.js"></script>
    <script src="../js/semantic.js"></script>
    <script src="../js/parallax.min.js"></script>
    <script src="../js/jquery.nicescroll.min.js"></script>
    <script src="../js/jquery.sticky.js"></script>
    <script src="../js/main.js"></script>
</div>
</body>
</html>